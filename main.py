import os
import random
import urllib
import webbrowser

import flickrapi
import utils.TwitterBot as TwitterBot
import keys.Key as Key
import utils.FileUtils as FileUtils
import utils.FlickrBot as FlickrBot

print("Step 1: authenticate")
flickr = FlickrBot.authentification()

print("Step 2: Get list pictures from account")
userID = FileUtils.readFile("./Key/userid.txt")
sets = flickr.photosets_getList(
    user_id=userID,
)

list_photos = []

for set in sets.getchildren()[0]:

    print("Getting Photos from set: " + set.getchildren()[0].text)

    for photo in flickr.walk_set(
        set.get("id"),
        extras="url_o",
    ):
        list_photos.append(photo.get("url_o"))

FileUtils.writeFile("photo/whole.txt", "\n".join(list_photos))

print("Step 3: Choose a random photography to publish")

pos = random.randrange(len(list_photos))
print("Choose the pic [" + str(pos) + "]: " + list_photos[pos])

print("Step 3: Download the photography")
urlToDownlaod = list_photos[pos]
filepathImage = "photo/toPublish/ToPublish" + "." + urlToDownlaod.split(".")[-1]
try:
    os.makedirs("photo/toPublish/", exist_ok=True)
    image = urllib.request.urlopen(urlToDownlaod).read()
    with open(filepathImage, "wb") as imgFile:
        imgFile.write(image)
        imgFile.close()
except Exception as e:
    print(e)

print("Step 4: Authentification on twitter")
twitter = TwitterBot.authentification()

print("Step 5: Publish on twitter")
TwitterBot.postMessage(
    message={"text": "📷 @Thomas_Auriel", "media": [filepathImage]},
    api=twitter,
    previousStatus=None,
    verbose=False,
)
