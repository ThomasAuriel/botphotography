import os

## Read the content of file
def readFile(filepath):

    try:
        fileAcces = open(filepath, "r")
        content = fileAcces.read()
        fileAcces.close()

        return content

    except:
        return None


## replace the content of file
def writeFile(filepath, content):

    os.makedirs(os.path.dirname(filepath), exist_ok=True)
    fileAcces = open(filepath, "w")
    fileAcces.write(content)
    fileAcces.close()
