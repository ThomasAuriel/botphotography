import webbrowser

import flickrapi

import utils.FileUtils as FileUtils
import keys.Key as Key

fileTokenFlickr = "./keys/TokenFlickr.txt"


def authentification():

    flickr = flickrapi.FlickrAPI(Key.flickr_Key, Key.flickr_secret)
    tokenFlickr = FileUtils.readFile(fileTokenFlickr)

    if tokenFlickr == None:

        # Only do this if we don't have a valid token already
        if not flickr.token_valid(perms="read"):

            # Get a request token
            flickr.get_request_token(oauth_callback="oob")

            # Open a browser at the authentication URL. Do this however
            # you want, as long as the user visits that URL.
            authorize_url = flickr.auth_url(perms="read")
            webbrowser.open_new_tab(authorize_url)

            # Get the verifier code from the user. Do this however you
            # want, as long as the user gives the application the code.
            tokenFlickr = str(input("Verifier code: "))

            FileUtils.writeFile(fileTokenFlickr, tokenFlickr)

    # Trade the request token for an access token
    flickr.token_valid(perms="read")
    flickr.get_access_token(tokenFlickr)

    return flickr
