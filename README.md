# BotPhotography

Copy a photography from Flickr to Twitter

## Installation

```sh
python3 -m pip install --user tweepy
python3 -m pip install --user flickrapi
```

## Coding rules

Before to submit a commit, apply :

```sh
autoflake -i -r --expand-star-imports --remove-all-unused-imports --remove-unused-variables ./scripts/*
black -l 100 *
```

To install formatters :

```sh
pip install --upgrade black
pip install --upgrade autoflake
```

## How to use it

### `Keys.py` file

To be able to use this code, a key file, containing API keys, is required. Two set of keys are required to use all the functions:

- Flickr Keys: They allow to use the flickr API
- Twitter keys: They allow posting messages on twitter from the bot scripts. You should create a twitter development account and follow instructions to create an app if you want to do the same.

Final `keys/Keys.py` file should look like this:

```python
# Flickr API keys
flickr_Key = "your-token"
flickr_secret = "your-token"

# Twitter API keys
consumer_key = "your-token"
consumer_secret = "your-token"
access_token = "your-token"
access_token_secret = "your-token"
```
